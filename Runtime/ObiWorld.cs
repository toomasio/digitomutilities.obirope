using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

namespace DigitomUtilities.ObiRopeTools
{
    [RequireComponent(typeof(ObiSolver))]
    [RequireComponent(typeof(ObiFixedUpdater))]
    public class ObiWorld : MonoBehaviour
    {
        public static ObiWorld Instance { get; private set; }

        private void Awake()
        {
            SetInstance();
        }

        private void SetInstance()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }
    }
}
