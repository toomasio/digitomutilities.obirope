using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

namespace DigitomUtilities.ObiRopeTools
{
    public class ObiWorldObject : MonoBehaviour
    {
        public enum ChildToType { OnAwake, OnStart }

        [SerializeField] private ChildToType childToObiWorld = default;

        private void Awake()
        {
            CheckChildToType(ChildToType.OnAwake);
        }

        private void Start()
        {
            CheckChildToType(ChildToType.OnStart);
        }

        private void CheckChildToType(ChildToType childToType)
        {
            if (childToObiWorld == childToType)
                ChildToObiWorld();
        }

        private void ChildToObiWorld()
        {
            if (!ObiWorld.Instance)
            {
                Debug.LogError($"{typeof(ObiWorld)} does not exist in this scene yet! Cannot child {this} to {typeof(ObiWorld)}");
                return;
            }

            transform.SetParent(ObiWorld.Instance.transform);
        }
    }
}
